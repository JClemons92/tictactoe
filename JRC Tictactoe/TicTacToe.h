#pragma once
#include <iostream>
#include <conio.h>

class TicTacToe {
private:

	// fields
	char m_board[9] = {' ',' ',' ',' ',' ',' ',' ',' ',' '}; // An array used to store the spaces of the game board. Once a player has Moved in one of the positions, the space should change to an X or an O.
	std::string m_wincheck[9]; // this will store the strings to check for winning positions
	int m_numTurns = 1; // This will be used to detect when the board is full (all nine spaces are taken).
	char m_playerTurn = 'x'; // This is used to track if it's X's turn or O's turn.
	char m_winner = ' '; // This is used to check to see the winner of the game. A space should be used while the game is being played.

public:

	// constructor
	TicTacToe() {};

	// This method will output the board to the console
	void DisplayBoard() const {
		std::cout << m_board[0] << " | " << m_board[1] << " | " << m_board[2] << '\n';
		std::cout << "__________" << '\n';
		std::cout << m_board[3] << " | " << m_board[4] << " | " << m_board[5] << '\n';
		std::cout << "__________" << '\n';
		std::cout << m_board[6] << " | " << m_board[7] << " | " << m_board[8] << '\n';
	};

	// Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
	bool IsOver() const { if (m_winner != ' ') { return 1; } else { return 0; } }

	// Returns an 'X' or an 'O,' depending on which player's turn it is.
	char GetPlayerTurn() const {if (m_playerTurn == 'x') { return '1'; } else {	return '2';	} }

	// Param: An int(1 - 9) representing a square on the board.Returns true if the space on the board is open, otherwise false.
	bool IsValidMove(int position) const { if (m_board[position-1] != ' ') { return 0; } else { return 1; }	}

	// Param: int (1 - 9) representing a square on the board. Places the current players character in the specified position on the board.
	void Move(int position) {
		// put move on board
		m_board[position-1] = m_playerTurn;

		// check for win
		WinCheck();

		//incriment turns
		m_numTurns++;

		// set next player
		if (m_playerTurn == 'x') {
			m_playerTurn = 'o';
		}
		else {
			m_playerTurn = 'x';
		}
	}

	//Displays a message stating who the winning player is, or if the game ended in a tie.
	void DisplayResult() const {
		if (m_winner == 't') {
			std::cout << "Tie game!" << '\n';}
		else {
			std::cout << m_winner << " wins the game!" << '\n';
		}
	}

	char WinCheck() {

		// check if max turn count, set tie, if last turn makes a winner, t is overwritten
		if (m_numTurns == 9) { m_winner = 't'; }

		// impossible to win before turn 5, dont check for wins
		if (m_numTurns >= 5) {

			// check for win horizontally
			m_wincheck[0] = std::string(1, m_board[0]) + m_board[1] + m_board[2];
			m_wincheck[1] = std::string(1, m_board[3]) + m_board[4] + m_board[5];
			m_wincheck[2] = std::string(1, m_board[6]) + m_board[7] + m_board[8];

			// check for win vertically
			m_wincheck[3] = std::string(1, m_board[0]) + m_board[3] + m_board[6];
			m_wincheck[4] = std::string(1, m_board[1]) + m_board[4] + m_board[7];
			m_wincheck[5] = std::string(1, m_board[2]) + m_board[5] + m_board[8];

			// check for win diagnally		
			m_wincheck[6] = std::string(1, m_board[0]) + m_board[4] + m_board[8];
			m_wincheck[7] = std::string(1, m_board[6]) + m_board[4] + m_board[2];

			// loop through combos to look for a winner
			for (int i = 1; i <= 8; i++) {

				// check for win
				if (m_wincheck[i] == "xxx" || m_wincheck[i] == "ooo") {
					std::string temp = m_wincheck[i];
					m_winner = temp[0];
				};
			}
		}
		
		// return winner if there is one, else 
		if (m_winner == ' ') { return ' ';}	else { return m_winner;	}
	}
};